import React, {useRef} from "react";
import ReactToPrint from 'react-to-print';
import './theme/generic.scss';
import './theme/normalize.scss';
import Header from "./sections/title/title";
import Achievements from "./sections/achievements/achievements";
import Highlights from "./sections/highlights/highlights";
import Education from "./sections/education/education";
import Experience from "./sections/experience/experience";
import {ReactComponent as PrintIcon} from './components/pictograms/print.svg';
import Certifications from "./sections/certifications/certifications";

export default function App() {
    const componentRef = useRef();
    return (
        <div className="main-container">
            <ReactToPrint
                trigger={() => <button className="print-button"><PrintIcon className="print-pictogram"/></button>}
                content={() => componentRef.current}
            />
            <div className="app" ref={componentRef}>
                <Header/>
                <Highlights/>
                <Achievements/>
                <Certifications/>
                <Education/>
                <Experience/>
            </div>
        </div>
    );
}

// TODO: fix print button
// TODO: implement image gallery for certificates (react-bnb-gallery)?
// TODO: investigate share image problem
