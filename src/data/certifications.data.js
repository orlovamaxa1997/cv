const title = 'certifications';

const data = [
    {
        title : "MP Academy: People partner",
        certFileName : "people-partner.pdf"
    }
];

export {title, data};
