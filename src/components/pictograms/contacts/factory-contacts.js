import {ReactComponent as PhoneIcon} from './phone.svg';
import {ReactComponent as EmailIcon} from './email.svg';
import {ReactComponent as SkypeIcon} from './skype.svg';
import {ReactComponent as TelegramIcon} from './telegram.svg';
import {ReactComponent as LinkedinIcon} from './linkedin.svg';
import {ReactComponent as GitIcon} from './github.svg';
import {ReactComponent as HabrIcon} from './habr.svg';

export default function getContactItemPictogram(type) {
    switch (type) {
        case "phone": return <PhoneIcon className="contact-item-icon"/>;
        case "email": return <EmailIcon className="contact-item-icon"/>;
        case "skype": return <SkypeIcon className="contact-item-icon"/>;
        case "telegram": return <TelegramIcon className="contact-item-icon"/>;
        case "linkedin": return <LinkedinIcon className="contact-item-icon"/>;
        case "git": return <GitIcon className="contact-item-icon"/>;
        case "toster": return <HabrIcon className="contact-item-icon"/>;
        default: return null;
    }
}