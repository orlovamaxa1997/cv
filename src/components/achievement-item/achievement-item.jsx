import React from "react";
import PropTypes from "prop-types";
import './achievement-item.scss'
import {ReactComponent as CheckmarkIcon} from "../pictograms/checkmark.svg";

export default function AchievementItem(props) {
    const {statement} = props;

    return (
        <div className="achievement-statement-container">
            <CheckmarkIcon className="list-checkmark"/>
            <p className="achievement-statement">{statement}</p>
        </div>
    );
}

AchievementItem.propTypes = {
        statement: PropTypes.string.isRequired
}