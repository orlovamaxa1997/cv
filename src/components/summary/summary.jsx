import './summary.scss';
import React from "react";
import summary from "../../data/summary.json";
import SectionHeader from "../section-header/section-header";

export default function Summary() {
    const {sectionTitle, data} = summary;

    function renderSummaryText(summary) {
        return summary.map((item, index) => {
            return (
                <p className="summary-paragraph" key={index}>{item}</p>
            )
        })
    }

    return (
        <section className="summary-container">
            <SectionHeader sectionTitle={sectionTitle}/>
            <div className="summary-text-container">
                {renderSummaryText(data)}
            </div>
        </section>
    );
}
