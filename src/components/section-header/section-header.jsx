import React from "react";
import PropTypes from "prop-types";
import './section-header.scss';

export default function SectionHeader(props) {
    const {sectionTitle, isClickable} = props;

    return (
        <h1 className="section-title">
            {sectionTitle}
            {(isClickable) ? <span className="section-items-clickable-sign">(clickable)</span> : null}
        </h1>
    );
}

SectionHeader.propTypes = {
    sectionTitle: PropTypes.string.isRequired,
    isClickable: PropTypes.bool
}