import React from "react";
import PropTypes from "prop-types";
import './education-item.scss'

export default function EducationItem(props) {
    const {title, specialisation, startYear, endYear} = props.content;

    return (
        <>
            <div className="education-item-container default-container-layout">
                <p className="education-period">{`${startYear} - ${endYear}`}</p>
                <div className="education-item-details">
                    <p className="education-item-title">{title}</p>
                    <p className="education-item-specialisation">{`Specialisation: ${specialisation}`}</p>
                </div>
            </div>
        </>
    );
}

EducationItem.propTypes = {
    content: PropTypes.shape({
        title: PropTypes.string.isRequired,
        specialisation: PropTypes.string.isRequired,
        startYear: PropTypes.number.isRequired,
        endYear: PropTypes.number.isRequired
    })
}
