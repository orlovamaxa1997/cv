import React from "react";
import SectionHeader from "../section-header/section-header";
import Contacts from "../../components/contacts/contacts";
import Languages from "../../components/languages/languages";

export default function Info() {
    return (
        <section>
            <SectionHeader sectionTitle="INFO" isClickable/>
            <Contacts />
            <Languages />
        </section>
    );
}
