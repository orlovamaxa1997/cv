import React from "react";
import './contacts.scss';
import contacts from "../../data/contacts.json";
import FContactIcons from '../pictograms/contacts/factory-contacts'

export default function Contacts() {
    const {data} = contacts;

    function renderContactItems(contacts) {
        return contacts.map((item, index) => {
            return (
                <div key={index} className="contact-item-container">
                    {FContactIcons(item.type)}
                    <a
                        href={item.href}
                        className="contact-item-content"
                        target={(["telegram", "linkedin", "git", "toster"].includes(item.type)) ? "_blank" : null}
                        rel="noreferrer"
                    >{item.content}</a>
                </div>
            )
        })
    }

    return (
        <div className="contacts-container">
            {renderContactItems(data)}
        </div>
    );

}
