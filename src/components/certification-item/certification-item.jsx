// MODULES
import React from "react";
import PropTypes from "prop-types";

// RESOURCES
import {ReactComponent as CheckmarkIcon} from "../pictograms/checkmark.svg";

// STYLES
import {certificationItemStyle} from "./certification-item.style";

export default function CertificationItem(props) {
    const {title, certFileName} = props.content;
    const style = certificationItemStyle();

    return (
        <div className="certification-item-container">
            <CheckmarkIcon className="list-checkmark"/>
            <a
                href={`./certificates/${certFileName}`}
                className={style.item}
                target="_blank"
                rel="noreferrer"
            >{title}</a>
        </div>
    );
};

CertificationItem.propTypes = {
    content: PropTypes.shape({
        title: PropTypes.string.isRequired,
        certFileName: PropTypes.string.isRequired
    })
}
