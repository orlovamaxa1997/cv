import {createUseStyles} from 'react-jss'

export const certificationItemStyle = createUseStyles({
    item: {
        transition: '0.3s',
        textDecoration: 'none',
        padding: '3px 0',
        color: 'inherit',
        flexGrow: 1,
    }
});