import React from "react";
import PropTypes from 'prop-types';
import './job.scss'
import JobSmartList from "../job-smart-list/job-smart-list";

export default function Job(props) {
    const {
        companyName,
        position,
        startYear,
        endYear,
        companyLogo,
        responsibilities,
        achievements
    } = props.content;

    return (
        <div id={`job-${companyName}`} className="job-container default-container-layout">
            <div className="job-label-container">
                {<p
                    className="job-cooperation-period">
                    {`${startYear}${(endYear) ? ` - ${endYear}` : ""}`}
                </p>}
                <img
                    src={`./img/logo/${companyLogo}`}
                    alt={`${companyName} logo`}
                    className="company-logo"
                />
            </div>
            <div>
                <p className="job-position">{position}</p>
                <JobSmartList title="Responsibilities" content={responsibilities}/>
                <JobSmartList title="Achievements" content={achievements}/>
            </div>
        </div>
    );
}

Job.propTypes = {
    content: PropTypes.shape({
        companyName: PropTypes.string.isRequired,
        companyLogo: PropTypes.string.isRequired,
        position: PropTypes.string.isRequired,
        startYear: PropTypes.number.isRequired,
        endYear: PropTypes.number,
        responsibilities: PropTypes.arrayOf(PropTypes.string).isRequired,
        achievements: PropTypes.arrayOf(PropTypes.string)
    })
}
