import React from "react";
import PropTypes from 'prop-types';
import './job-smart-list.scss'

export default function JobSmartList(props) {
    const {title, content} = props;

    if (content.length === 0) return null;

    return (
        <>
            <p className="job-smart-list-header">{`${title}:`}</p>
            <ul className="job-smart-list">
                {content.map((item, index) => <li
                    key={index}
                >{item}
                </li>)}
            </ul>
        </>
    );
}

JobSmartList.propTypes = {
    achievementsList: PropTypes.arrayOf(PropTypes.string)
}