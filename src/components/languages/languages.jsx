import React from "react";
import './languages.scss';
import clsx from "clsx";
import {ReactComponent as CircleIcon} from '../pictograms/circle.svg';
import languages from "../../data/languages.json"

export default function Languages() {
    const {data} = languages;

    function renderLanguageSkills(languages) {
        return languages.map((item, index) => {
            return (
                <div className="language-container" key={index}>
                    <p className="language-title">{item.language}</p>
                    <div className="language-rating-container">
                        {renderLanguageGrade(item.grade)}
                    </div>
                </div>
            )
        })
    }

    function renderLanguageGrade(grade) {
        const ratingElements = [];
        for (let i = 0; i < 5; i++) {
            ratingElements.push(
                <CircleIcon
                    key={i}
                    className={clsx("language-rating-point", {"language-rating-point-active": i < grade})}
                />
            )
        }
        return ratingElements;
    }

    return (
        <>
            <div className="languages-container">
                {renderLanguageSkills(data)}
            </div>
        </>
    );
}
