import React from "react";
import './title.scss';
import header from '../../data/header.json';

export default function Title() {

    function renderInterests(interests) {
        return interests.map((item, index) => {
            return <span className="title-interests" key={index}>{item}</span>
        })
    }

    return (
        <div className="title-container">
            <div className="title-info-container">
            <h1 className="title-text">{header.data.name}</h1>
            {renderInterests(header.data.interests)}
            </div>
            <img src="./img/header-qr.png" className="title-qr-code" alt="QR code"/>
        </div>
    );
}
