import React from "react";
import SectionHeader from "../../components/section-header/section-header";
import AchievementItem from "../../components/achievement-item/achievement-item";
import achievements from "../../data/achievements.json";
import "./achievements.scss";

export default function Achievements() {
    const {sectionTitle, data} = achievements;

    return (
        <section className="achievements-container">
            <SectionHeader sectionTitle={sectionTitle}/>
            {data.map((item, index) => <AchievementItem statement={item} key={index}/>)}
        </section>
    );
}
