import React from "react";
import SectionHeader from "../../components/section-header/section-header";
import EducationItem from "../../components/education-item/education-item";
import education from "../../data/education.json";

export default function Education() {
    const {sectionTitle, data} = education;

    return (
        <section>
            <SectionHeader sectionTitle={sectionTitle}/>
            {data.map((item, index) => <EducationItem content={item} key={index}/>)}
        </section>
    );
}
