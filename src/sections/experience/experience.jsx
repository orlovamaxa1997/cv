import React from "react";
import jobs from "../../data/jobs.json";
import SectionHeader from "../../components/section-header/section-header";
import Job from "../../components/experience/job/job";

export default function Experience() {
    const {sectionTitle, data} = jobs;

    return (
            <section>
                <SectionHeader sectionTitle={sectionTitle}/>
                {data.map((item, index) => <Job content={item} key={index}/>)}
            </section>
    );
}
