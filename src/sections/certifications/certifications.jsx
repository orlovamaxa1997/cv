// MODULES
import React from "react";

// RESOURCES
import SectionHeader from "../../components/section-header/section-header";
import CertificationItem from "../../components/certification-item/certification-item";
import {title, data} from "../../data/certifications.data";

// STYLES
import './certifications.scss';

export default function Certifications() {

    return (
        <section>
            <SectionHeader sectionTitle={title} isClickable/>
            <div className="certification-items-container">
                {data.map((item, index) => <CertificationItem content={item} key={index}/>)}
            </div>
        </section>
    );
}