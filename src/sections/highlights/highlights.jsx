import React from "react";
import Info from "../../components/info/info";
import Summary from "../../components/summary/summary";

export default function Highlights() {
    return (
        <section className="default-container-layout">
            <Info/>
            <Summary/>
        </section>
    );
}